//Imports
const express = require('express')
const cors = require('cors')
const cookieSession = require('cookie-session')
const path = require('path')
const ejs = require('ejs')
const app = express()
const http = require('http')
const https = require('https')
const fs = require('fs')
var key = fs.readFileSync(__dirname + '/server.key')
var cert = fs.readFileSync(__dirname + '/server.cert')
const {writeFile} = require('fs')
const Metrics = require('./metrics')
const { createAdapter } = require('@socket.io/redis-adapter');
const { createClient } = require('redis');

const pubClient = createClient({ host: 'redis', port: 6379 });
const subClient = pubClient.duplicate();




const credentials = {
  key: key,
  cert: cert
}

const server = http.createServer(credentials,app);
const { Server } = require("socket.io");
const { count } = require('console')
const io = new Server(server);

io.adapter(createAdapter(pubClient, subClient));

//Setting up Cross Origin
var corsOption = {
  origin: "http://localhost:3001"
}

//Setting up Middle to use
app.use(cors(corsOption))

app.use(express.json())

app.use(express.urlencoded({ extended: true }));

app.use(
    cookieSession({
      name: "Cloud-Chat-User",
      secret: "faSFasF12131!",
      httpOnly: true
    })
  );

 
app.set(path.join(__dirname, '/app'))
//setting the view engine to html
app.engine('html', ejs.renderFile)
app.set('view engine', 'html')
//setting the path
app.use(express.static(path.join(__dirname, "/app")));


//importing the user model
const db = require(path.join(__dirname,"/app/models"))

//connceting to the Database
db.mongoose
  .connect(`mongodb+srv://cici:cloudcomputing@cloud-computing.cl9pyt6.mongodb.net/?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Successfully connect to MongoDB.");
  })
  .catch(err => {
    console.error("Connection error", err);
    process.exit();
  });



//setting up the endpoints
app.get('/start', (req,res) =>{
    res.render('register.html')
})

app.get('/login', (req,res) =>{
  res.render('login.html')
})

app.get('/index', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

//connecting to the socket 
io.on('connection', (socket) => {
  socket.on('chat message', (msg) => {
    Metrics.msgSent.inc()
    //this returns the count of words for every msg sent
    var length = msg.split(' ').filter(word=> word !=='').length
    Metrics.msgLength.observe(length)
    io.emit('chat message', msg);
  });
  socket.on("upload", (file, callback) => {
    console.log(file);
    writeFile("/tmp/upload", file, (err) => {
      callback({ message: err ? "failure" : "success" });
    });
  });

});





//Importing routes
require(path.join(__dirname,'/app/routes/auth.routes'))(app);
require(path.join(__dirname,'/app/routes/user.routes'))(app);


//exporting the collected metrics to prometheus metrics endpoint 
app.get('/metrics', async (req, res) => {
  res.set('Content-Type', Metrics.register.contentType)
  res.end(await Metrics.register.metrics())
})

server.listen(3000, () => {
  console.log('Server running on port 3000')
})







