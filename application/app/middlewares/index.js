const path = require('path')
const authJwt = require(path.join(__dirname,"/authJWT"));
const verifySignUp = require(path.join(__dirname,"/verifySignUp"));

module.exports = {
  authJwt,
  verifySignUp
};