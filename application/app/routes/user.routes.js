const path = require('path')
const { authJwt } = require(path.join(__dirname,"../middlewares"))
const controller = require(path.join(__dirname,"../controllers/user.controller"))

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/test/all", controller.allAccess);

  app.get("/chat", [authJwt.verifyToken], controller.userBoard);

};