const path = require('path')
const db = require(path.join(__dirname,"../models"))
const User = db.user;


//Checks for Duplicated Usernames or emails during registration
//At our Database we lookup for existing entries with the username or email. If one exists we return the fitting error
checkDuplicateUsernameOrEmail = (req, res, next) => {
  // Username
  console.log(req.body)
  User.findOne({
    username: req.body.username
  }).exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    if (user) {
      res.status(400).send({ message: "Failed! Username is already in use!" });
      return;
    }

    // Email
    User.findOne({
      email: req.body.email
    }).exec((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }

      if (user) {
        res.status(400).send({ message: "Failed! Email is already in use!" });
        return;
      }

      next();
    });
  });
};


const verifySignUp = {
  checkDuplicateUsernameOrEmail
};

module.exports = verifySignUp;