const client = require('prom-client')

//creating a registry where the metrics are collected 
const register = new client.Registry()

register.setDefaultLabels({
  app: 'cloud-computing-chat'
})

//this enables collecting the defaul metrics the prom-client for node offers 
client.collectDefaultMetrics({register})

//here the metrics are defined 
const msgSent = new client.Counter({
    name: 'msgSent',
    help: 'total number of messages sent'
  })
  
const msgLength = new client.Histogram({
    name: 'msgLength',
    help: 'length of message sent',
    buckets: [1, 3, 5, 8, 10, 15, 20, 30, 40, 50, 70]
  })
  
const registercounter = new client.Counter({
    name: 'usercounter',
    help: 'total number of registered users'
  })

const onlineUser = new client.Gauge({
  name: 'onlineUser',
  help: 'total number of current online users'
})

const failedLogins = new client.Counter({
  name: 'failedLogins',
  help: 'number of failed logins'
})
  
//this registers the metrics in the registry we have created 
register.registerMetric(registercounter)
register.registerMetric(msgSent)
register.registerMetric(msgLength)
register.registerMetric(onlineUser)
register.registerMetric(failedLogins)

//exports the registry and the metrics to apply changes to metrics. e.g. inc the count for msgSent
exports.register = register
exports.msgSent = msgSent
exports.msgLength = msgLength
exports.onlineUser = onlineUser
exports.registercounter = registercounter
exports.failedLogins = failedLogins