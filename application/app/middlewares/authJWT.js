const path = require('path')
const jwt = require("jsonwebtoken");
const config = require(path.join(__dirname,"../config/auth.config.js"))
const db = require(path.join(__dirname,"../models"))
const User = db.user;


//Here we check if the token created at the signup is correct. We need this to access whenever we request data we need to be logged in
verifyToken = (req, res, next) => {
  let token = req.session.token;

  if (!token) {
    return res.status(403).send({ message: "No token provided!" });
  }

  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({ message: "Unauthorized!" });
    }
    req.userId = decoded.id;
    next();
  });
};

const authJwt = {
  verifyToken
};
module.exports = authJwt;