const path = require('path')
const config = require(path.join(__dirname, "../config/auth.config"))
const db = require(path.join(__dirname, "../models"))
const User = db.user
const Metrics = require('../../metrics')

var jwt = require("jsonwebtoken")
var bcrypt = require("bcryptjs")
const { Server } = require('http')


//Here we signup an user. The Password provided during signup process gets encrypted and saved into the database
exports.signup = (req, res) => {
  console.log(req.body)
  const user = new User({
    username: req.body.username,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8),
  });


  user.save((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }
    Metrics.registercounter.inc()
    res.redirect('/login');
  });
};

//Signing in we compare the hased passwort from the database with the password provided while signing in. 
//If it matches we sign an token that expires after 24 hours
exports.signin = (req, res) => {
  User.findOne({
    username: req.body.username,
  })
    .exec((err, user) => {
      if (err) {
        Metrics.failedLogins.inc()
        res.status(500).send({ message: err });
        return;
      }

      if (!user) {
        Metrics.failedLogins.inc()
        return res.status(404).send({ message: "User Not found." });
      }

      var passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );

      if (!passwordIsValid) {
        Metrics.failedLogins.inc()
        return res.status(401).send({ message: "Invalid Password!" });
      }

      var token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: 86400, // 24 hours
      });


      req.session.token = token;
      Metrics.onlineUser.inc()
      res.redirect('/chat')
    });
};

//Signing out
exports.signout = async (req, res) => {
  try {
    req.session = null;
    Metrics.onlineUser.dec()
    res.redirect('/login')
  } catch (err) {
    this.next(err);
  }
};