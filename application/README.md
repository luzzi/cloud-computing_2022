## Documentation

Socket.io Chatroom for Cloud-Computing
===

Diagramm provided in the repository.
Code is commentet.

Used technologies: Node.js, Express, Mongoose, Socket.io, JWT, Cookie-Session, ejs, bcrypt


Installation
===

#### 1. Clone the Repository

```sh
git clone https://gitlab.reutlingen-university.de/luzzi/cloud-computing_2022.git

```

#### 2. Install the Dependencies

```sh
npm install
```

#### 3. Start the Server

```sh
npm start
```

> View on localhost:3000/start


### 4. Changes from Excercise 2

#### Create an Image

To create an Image of the Application navigate to the application folder where the Dockerfiles is located.
Then run following command: 


```sh
docker build -t <image-tag> . 
```

#### Run a Container with the Image 

```sh
docker run --name <containerName> <imagename> 
```

The Application is now running on port 3000<br>

#### Starting the application and Prometheus

To start the Application and Prometheues run the following command:<br>

```sh
docker compose up
```

The Application is now running on Port 3000.<br>
Prometheus is running on Port 9091. <br>


#### Visualizing different Metrics on Prometheus 

To Visiualize different Metrics go to localhost:9091/graph <br>
 
You can plot different Metrics by searching for the following Metrics: <br>

registercounter <br>
msgSent <br>
msgLength <br>
onlineUser <br>
failedLogins <br>


### Changes from Exercise 3

#### Add an nginx Configuration File

The Nginx Configuration File determines how nginx work. In that config file we specify what functionalities from nginx we want to use. 
In The Case of this Exercise the http directive is the main thing to look at. 
<br>
In the Server block we specify the port and the location block which defines the proxy that handles the requests, loadbalance them and forward them to one of the nodes.
<br>
The Second Block is the upstream_nodes block in which the method for load balancing is defined, in this case its based on ip hash. Besides that the 4 nodes are defined which are all instances of the web app. 



#### Configure the Docker Compose File

The Docker Compose File is now enhancend in a way that instead of one container for the app it starts 4 container running the app each. 
Additional we have a container wich runs the nginx logic and one for the redis db. The prometheus container remains the same. 


#### Add the Adapter 

To ensure that every packet is sent to every matching client in the case of an packet that is sent to multiple client we used the redis adapter for socket.io. 




